# VLV 2

> État: vérifications et nettoyage encore en cours. Le fichier final anonymisé sera transmis à FORS (SWISSUbase).

«VLV2» est une enquête longitudinale (seconde vague), basée sur «[Vivre - Leben - Vivere (VLV)](https://cigev.unige.ch/vlv/)», une enquête transversale réalisée en 2011-2012 par le CIGEV (Centre interfacultaire de gérontologie et d'études des vulnérabilités). Cette seconde vague d'entretiens a été réalisée par l'institut LINK pour le compte du CIGEV. Les données ont été récoltées dans les cantons de Bâle, Berne, Genève et Valais.

Ce projet a été financé par le Pôle de recherche national «LIVES – Surmonter la vulnérabilité, perspectives du parcours de vie», un programme du Fonds national suisse de la recherche scientifique ([FNS](http://www.snf.ch/)).

## Pour commencer

Nous proposons ici un jeu de données mis à jour et corrigé depuis 2020, ainsi qu'une documentation complète. La liste des fichiers disponibles est [détaillée ci-dessous](#contenu), et la documentation se trouve dans le fichier [long_2017_doc.md](long_2017_doc.md).

En clonant ce dépôt, vous vous engagez à respecter la [licence d'utilisation](LICENSE) et à citer [les auteur·e·s](#auteures) et le titre de l'enquête ([voir la citation proposée](#citation)).

### Citation

(FR)
> Kliegel, M. & Oris, M. (dir.). (2017). *VLV2* [Jeu de données et documentation]. Tiré de https://gitlab.unige.ch/cigev/

(EN)
> Kliegel, M. & Oris, M. (Dir.). (2017). *VLV2* [Data file and documentation]. Retrieved from https://gitlab.unige.ch/cigev/

### Prérequis

Vous pouvez télécharger les données fournies par le CIGEV et les utiliser librement, selon la [licence d'utilisation](LICENSE). Afin de faire profiter les autres utilisateurs/trices de vos corrections éventuelles, utilisez Git: voir le livre [Pro Git](https://git-scm.com/book/fr/v2) pour bien commencer.

Le [jeu de données](long_2017_data.csv) est fourni au format [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values), mais peut être importé dans [SPSS](https://fr.wikipedia.org/wiki/SPSS) grâce à la [syntaxe d'importation](long_2017_data.sps) qui l'accompagne. La [documentation](long_2017_doc.md) est fournie sous la forme d'un fichier [MD](https://fr.wikipedia.org/wiki/Markdown). Le [codebook](long_2017_codebook.ods) a été exporté depuis SPSS et est fourni au format [OpenDocument](https://fr.wikipedia.org/wiki/OpenDocument). Les questionnaires sont au format [PDF](https://fr.wikipedia.org/wiki/Portable_Document_Format).

### Installation

Vous pouvez copier le jeu de données et la documentation en le téléchargeant depuis ce site (voir lien «Download» en haut à droite de la liste des fichiers) ou en clonant le dépôt:

```
git clone git@gitlab.unige.ch:cigev/long_2017_vlv2.git
```

Pour importer les données dans SPSS, utilisez la syntaxe fournie (long_2017_data.sps) et modifiez la ligne appelant le fichier CSV en indiquant le chemin complet du fichier.

## Contenu

* [LICENSE](LICENSE) – Licence d'utilisation des données
* [README.md](README.md) – Indications de base (ce fichier)
* xxx

## Contribuer

Si vous désirez contribuer à l'amélioration de ce jeu de données, de sa documentation, ou laisser des commentaires, vous pouvez contacter le CIGEV à l'adresse: [cigev-data@unige.ch](mailto:cigev-data@unige.ch). Le système Git vous permettra de proposer des modifications que nous pourrons valider, documenter et partager.

Pour toute utilisation des données (analyse, comparaison, enseignement, publication...), nous vous serions reconnaissants de bien vouloir nous informer: [cigev-data@unige.ch](mailto:cigev-data@unige.ch).

## Auteur·e·s

* **Matthias Kliegel** – *Requérant principal*
* **Michel Oris** – *Requérant principal*

### Collaboratrices et collaborateurs

* **Marie Baeriswyl**
* **Nicola Ballhausen**
* **Delphine Fagot**
* **Rainer Gabriel**
* **Elisa Gallerne**
* **Andreas Ihle**
* **Fanny Vallet**

### Administration et technique

* **Nathalie Blanc**
* **Claire Grela**
* **Grégoire Métral**

## Licence

Ce projet est distribué selon les termes de la licence de CreativeCommons CC-BY-SA. Voir le fichier [LICENSE](LICENSE) pour plus de détails.

## Remerciements

Merci à toutes les personnes (notamment les enquêtrices et enquêteurs) qui ont également collaboré à cette recherche.
